from dylos_serial_pi import *
from Tkinter import *
import tkFileDialog
import threading
import time
from ConfigParser import SafeConfigParser
import sys
import os
import pickle
import requests
import datetime

# Useful timeouts
import eventlet
eventlet.monkey_patch(socket=True)

killall = False

# The Jinmoti of Bozlen Two kill the hereditary
# ritual assassins of the new Yearking's immediate
# family by drowning them in the tears of the
# Continental Empathaur in its Sadness Season.

def dlog(*args):
    for x in args:
        try:
            print x
        except:
            print "Exception: tried to print an argument but couldn't"
        try:
            with open("/home/pi/dylospi/error_log.txt", 'a') as error_log:
                now = datetime.datetime.now().strftime("Logged: %d/%m/%y %H:%M:%S --> ")
                error_log.write(str(now))
                error_log.write(str(x) + os.linesep)
        except Exception as e:
            try:
                print "Exception when trying to write to error log - perhaps we're not on a raspberry pi?"
                print e
                return False
            except:
                print "Additionally, an exception occured trying to print the original exception."
                return False
    return True




# target_url = "https://ruaraidhdobson.pythonanywhere.com"
datastore = []
connection_problem_count = 0 # If this gets to a threshold (in settings), we're rebooting


def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo','r')
        for line in f:
            if line[0:6]=='Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"
    return cpuserial


def write_settings(dylos_id=False, api_key=False, k_factor=False, reboot_minutes=False, target=False, local_log_location=False):
    dlog("writing settings")
    scp = SafeConfigParser()
    current_file = os.path.realpath(__file__)
    settings_file = os.path.dirname(current_file) + "/settings.ini"
    print settings_file
    dlog("writing settings to file", settings_file)
    try:
        scp.add_section("Settings")
    except:
        pass
    if dylos_id is not False:
        scp.set("Settings", "Dylos ID", dylos_id)
    if api_key:
        scp.set("Settings", "API Key", api_key)
    ser_number = getserial()
    scp.set("Settings", "Pi Serial Number", ser_number)
    if k_factor:
        scp.set("Settings", "K Factor", k_factor)
    if reboot_minutes is not False:
        scp.set("Settings", "Reboot After Network Loss", reboot_minutes)
    if target is not False:
        scp.set("Settings", "Target URL", target)
    if local_log_location is not False:
        scp.set("Settings", "Local Log Location", local_log_location)
    with open(settings_file, 'wb') as settings_ini:
        scp.write(settings_ini)
    return True


def load_settings():
    dlog("loading settings")
    scp = SafeConfigParser()
    current_file = os.path.realpath(__file__)
    settings_file = os.path.dirname(current_file) + "/settings.ini"
    print settings_file
    dlog("loading settings from file:", settings_file)
    # scp.read("settings.ini")
    scp.read(settings_file)
    try:
        dylos_id = scp.get("Settings", "Dylos ID")
    except:
        dylos_id = "Unknown Dylos"
    try:
        api_key = scp.get("Settings", "API Key")
    except:
        api_key = ""
    try:
        cpuserial = scp.get("Settings", "Pi Serial Number")
    except:
        cpuserial = ""
    try:
        k_factor = float(scp.get("Settings", "K Factor"))
    except:
        k_factor = 1.00
    try:
        reboot_on_network_loss = int(scp.get("Settings", "Reboot After Network Loss"))
    except:
        reboot_on_network_loss = 0
    try:
        target = str(scp.get("Settings", "Target URL"))
    except:
        target = "https://ruaraidhdobson.pythonanywhere.com"
    try:
        local_log_location = str(scp.get("Settings", "Local Log Location"))
    except:
        local_log_location = "local_data.csv"
    return (dylos_id, api_key, cpuserial, k_factor, reboot_on_network_loss, target, local_log_location)


def create_equation_dialog():
    equation_dialog = Toplevel(window)

    frame = Frame(equation_dialog)
    frame.pack(expand=1, fill=X)

    eq_type_var = StringVar(window)
     


def create_settings_dialog():
    settings_dialog = Toplevel(window)

    id_var = StringVar(window)
    id_var.set(str(settings[0]))

    frame = Frame(settings_dialog)
    frame.pack(expand=1, fill=X)

    id_frame = Frame(frame)
    id_frame.pack(expand=1, fill=X)

    id_label = Label(id_frame, text="ID")
    id_label.pack(side=LEFT)

    id_box = Entry(id_frame, textvariable=id_var)
    id_box.pack(side=LEFT, expand=1, fill=BOTH)

    api_frame = Frame(frame)
    api_frame.pack(expand=1, fill=BOTH)

    api_label = Label(api_frame, text="API key")
    api_label.pack(side=LEFT)

    api_key_var = StringVar(window)
    api_key_var.set(str(settings[1]))
    api_key_box = Entry(api_frame, textvariable=api_key_var)
    api_key_box.pack(side=LEFT, expand=1, fill=BOTH)

    k_frame = Frame(frame)
    k_frame.pack(expand=1, fill=BOTH)

    k_label = Label(k_frame, text="k factor")
    k_label.pack(side=LEFT)

    k_var = StringVar(window)
    k_var.set(str(settings[3]))
    k_var_box = Entry(k_frame, textvariable=k_var)
    k_var_box.pack(side=LEFT, expand=1, fill=BOTH)

    reboot_frame = Frame(frame)
    reboot_frame.pack(expand=1, fill=BOTH)
    reboot_label = Label(reboot_frame, text="Reboot after how many failed contact attempts (0 for never)")
    reboot_label.pack(side=LEFT)

    reboot_var = StringVar(window)
    reboot_var.set(str(settings[4]))
    reboot_var_box = Entry(reboot_frame, textvariable=reboot_var)
    reboot_var_box.pack(side=LEFT, expand=1, fill=BOTH)

    target_frame = Frame(frame)
    target_frame.pack(expand=1, fill=BOTH)

    target_label = Label(target_frame, text="Target URL")
    target_label.pack(side=LEFT)

    target_var = StringVar(window)
    target_var.set(str(settings[5]))
    target_var_box = Entry(target_frame, textvariable=target_var)
    target_var_box.pack(side=LEFT, expand=1, fill=BOTH)

    location_frame = Frame(frame)
    location_frame.pack(expand=1, fill=BOTH)

    location_label = Label(location_frame, text="Local log location")
    location_label.pack(side=LEFT)

    location_var = StringVar(window)
    location_var.set(str(settings[6]))
    location_var_box = Entry(location_frame, textvariable=location_var)
    location_var_box.pack(side=LEFT, expand=1, fill=BOTH)
    location_var_button = Button(location_frame, text="Browse", command=lambda: choose_local_location(settings_dialog, location_var))
    location_var_button.pack()

    save_settings_button = Button(frame, text="Save settings", command=lambda: on_save_click(settings_dialog, id_var, api_key_var, k_var, reboot_var, target_var, location_var))
    save_settings_button.pack()


def choose_local_location(frame, current_location):
    chosen_location = tkFileDialog.asksaveasfilename(defaultextension=".csv")
    frame.lift()
    if chosen_location:
        current_location.set(chosen_location)
        return str(chosen_location)
    else:
        return current_location.get()


def create_about_dialog():
    global version
    about_dialog = Toplevel()
    about_dialog.title("About RAPID")


def save_to_local_log(data_dict):
    dlog("saving to local log")
    csv_loc = settings[6]
    if not os.path.exists(csv_loc):
        with open(csv_loc, 'wb') as log_csv:
            fieldnames = data_dict.keys()
            writer = csv.DictWriter(log_csv, fieldnames=fieldnames)
            writer.writeheader()
    with open(csv_loc, 'a') as log_csv:
        fieldnames = data_dict.keys()
        writer = csv.DictWriter(log_csv, fieldnames=fieldnames)
        writer.writerow(data_dict)
    with open("local_log.txt", "a") as log_file:
        log_file.write(str(data_dict) + os.linesep)
    dlog("saved to local log")
    return True


def reboot_pi(text_widget, extra_message=None):
    global datastore
    global connection_problem_count
    dlog("Rebooting!")
    text_widget.insert(END, "The network may be down. Rebooting system.\n")
    if extra_message:
        text_widget.insert(END, str(extra_message))
    transmit_flag.set()
    # time.sleep(6)
    # text_widget.insert(END, "Transmitter should be dead.\n")
    try:
        with open("/home/pi/dylospi/datastore.BACKUP", 'wb') as datastore_backup:
            pickle.dump(datastore, datastore_backup)
        text_widget.insert(END, "datastore backed up to datastore.BACKUP. Will be loaded on next run.\n")
    except:
        text_widget.insert(END, "Failed to backup datastore. The following data may be lost: " + str(datastore) + "\n")
    now = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
    log_file = "REBOOT_" + now + ".log"
    dlog("associated log file:", log_file)
    text_widget.insert(END, "Dumping text_widget contents to log file: " + log_file + "\n")
    text_widget.insert(END, "Sending request for system reboot. So long, and thanks for all the fish.\n")
    text = text_widget.get("1.0", END)
    try:
        with open(log_file, 'wb') as open_log:
            open_log.write(text)
    except Exception as e:
        try:
            dlog("couldn't dump text to the reboot log for some reason")
            text_widget.insert(END, "Couldn't dump text to log file for some reason: " + str(e))
        except:
            text_widget.insert(END, "Couldn't dump text to log file for some reason. Additionally an exception occurred trying to write the exception string to the text widget.")
    os.system("sudo /sbin/shutdown -r now")
    sys.exit(1)


def transmitter(text_widget):
    global datastore
    global connection_problem_count
    # global target_url
    target_url = settings[5]
    if target_url == "":
        text_widget.insert(END, "RAPID is in local mode. Data is not being sent to a server. Enter a target URL in settings to change this.\n")
    print "transmitter started"
    dlog("starting transmitter thread")
    while True:
        print "main transmitter loop"
        if flag.is_set() or transmit_flag.is_set():
            print "Transmitter dying\n"
            dlog("transmit thread dying due to flag")
            text_widget.insert(END, "Transmitter dying.\n")
            return
        if datastore == []:
            print "nothing in datastore"
            time.sleep(5)
        else:
            dlog("datastore contains data...")
            for data_dict in datastore:
                try:
                    # print "trying to post data"
                    dlog("trying to post data")
                    result = eventlet.timeout.with_timeout(10, requests.post, target_url, json=data_dict, timeout=5, verify=False, timeout_value=None)
                    # result = requests.post(target_url, json=data_dict, timeout=5)
                    print result
                    if result == None:
                        dlog("raising exception...")
                        raise Exception("Exception - can't connect")
                    text_widget.insert(END, str(result) + "\n")
                    connection_problem_count = 0
                    datastore.remove(data_dict)
                    # time.sleep(5)
                except Exception as e:
                    try:
                        dlog("couldn't post data")
                        print e
                    except:
                        print "couldn't print exception"
                    if settings[4] == 0:
                        text_widget.insert(END, "Ignoring failed connection attempt. Set the reboot count > 0 to change this.\n")
                    else:
                        try:
                            text_widget.insert(END, str(e) + "\n")
                        except:
                            text_widget.insert(END, "Couldn't insert str(e)" + "\n")
                        connection_problem_count += 1
                        dlog("connection problem has occurred, with total standing at", connection_problem_count)
                        text_widget.insert(END, "Raspberry Pi will reboot if " + str(settings[4]) + " connection errors occur. Currently at: " + str(connection_problem_count) + "\n")
                    time.sleep(5)
        print "connection_problem_count: " + str(connection_problem_count)
        if settings[4] > 0:
            if connection_problem_count >= int(settings[4]):
                # print "Uh oh, connection problems!"
                dlog("connection problems greater than set number have occurred, rebooting...")
                # threading.Thread(target=reboot_pi, args=(text_widget))
                reboot_pi(text_widget)
                return


def run(port, dylos_id="Unknown Dylos", text_widget=None, api_key="", k_factor=1.00):
    global datastore
    global connection_problem_count
    try:
        ser = serial.Serial(port, timeout=300)
    except serial.SerialException as e:
        try:
            text_widget.insert(END, "Failure: " + str(e))
            dlog("failed to connect to Dylos")
        except:
            text_widget.insert(END, "Failure plus additional failure in inserting str(e)")
        text_widget.insert(END, "\nMake sure the Dylos is plugged in and turned on, and then restart this app.\n")
        return
    while True:
        if flag.is_set():
            print "Dying..."
            text_widget.insert(END, "Thread dying...")
            dlog("Run thread dying due to flag")
            button.config(state="normal")
            return
        try:
            raw_data = ser.readline()
        except serial.SerialException as e:
            try:
                print "Can't read, trying again..."
                dlog("Error reading from Dylos")
                print e
            except:
                print "Couldn't print exception data"
        print "raw_data: " + str(raw_data)
        if raw_data == "":
            text_widget.insert(END, "No reading detected - is a Dylos properly connected? Rebooting.\n")
            dlog("No reading detected - is a Dylos properly connected? Rebooting.")
            reboot_pi(text_widget)
        try:
            data = [int(x.strip())*100 for x in raw_data.split(",")]
        except ValueError as e:
            try:
                print e
            except:
                print "couldn't print e"
            continue
##        print data
##        print round(calculate_PM25_equiv(data[0] - data[1]),2)
        pm_equiv = round(calculate_PM25_equiv(data[0] - data[1]),2)
        k_pm = float(k_factor) * pm_equiv # Apply k factor
        try:
            lrg_prt_prc = 100*data[1]/data[0]
        except Exception as e:
            lrg_prt_prc = 0
        data_dict = {"small particles": data[0],
                     "large particles": data[1],
                     "Dylos ID": dylos_id,
                     "datetime": datetime.datetime.now().strftime("%d/%m/%y %H:%M"),
                     "PM2.5 equivalent": str(k_pm),
                     "large particle percentage": lrg_prt_prc,
                     "timestamp": str(time.time()),
                     "pi serial number": settings[2],
                     "api key": api_key}
##        print data_dict
        print json.dumps(data_dict)
        if text_widget is not None:
            text_widget.insert(END, str(json.dumps(data_dict)) + "\n")
        # try:
        #     result = requests.post("https://ruaraidhdobson.pythonanywhere.com", json=data_dict)
        #     print result
        #     text_widget.insert(END, str(result) + "\n")
        #     connection_problem_count = 0
        # except Exception as e:
        #     print e
        #     text_widget.insert(END, str(e) + "\n")
        #     connection_problem_count += 1
        #     text_widget.insert(END, "Raspberry Pi will reboot if 5 connection errors occur. Currently at: " + str(connection_problem_count) + "\n")
        datastore.append(data_dict)
        data_dict["pre-k factor PM2.5 equivalent"] = str(pm_equiv)
        save_to_local_log(data_dict)
        text_widget.see(END)
        # print "connection_problem_count: " + str(connection_problem_count)
        # if connection_problem_count >= 2:
        #     threading.Thread(target=reboot_pi, args=(text_widget))
            # reboot_pi(text_widget)
            # os.system("sudo /sbin/shutdown -r now") # Simplistic reboot command

def on_go_click():
    flag.clear()
    text.insert(END, "Running...\n")
    dlog("Running")
    settings = load_settings()
    threading.Thread(target=transmitter, args=[text]).start()
    threading.Thread(target=run, args=(choice_var.get(), settings[0], text, settings[1], settings[3]) ).start()
    button.config(state="disabled")
    stop_btn.config(state="normal")
    # id_box.config(state="disabled")
    # api_key_box.config(state="disabled")
    # k_var_box.config(state="disabled")


def on_stop_click():
    flag.set()
    text.insert(END, "Stopping: waiting for final read...\n")
    dlog("Stopping, waiting for final read")
    stop_btn.config(state="disabled")
    # id_box.config(state="normal")
    # api_key_box.config(state="normal")
    # k_var_box.config(state="normal")

def on_save_click(settings_dialog, id_var, api_key_var, k_var, reboot_var, target_var, local_var):
    global settings
    id_save = id_var.get()
    api_key_save = api_key_var.get()
    k_factor_save = k_var.get()
    reboot_save = reboot_var.get()
    target_save = target_var.get()
    local_save = local_var.get()
    write_settings(id_save, api_key_save, k_factor_save, reboot_save, target_save, local_save)
    settings = load_settings()
    settings_dialog.destroy()


if __name__ == "__main__":
    global datastore
    dlog("Good morning! I'm starting up...")
    try:
        with open("/home/pi/dylospi/VERSION", "rb") as version_file:
            version = version_file.read()
    except:
        try:
            with open("VERSION", 'rb') as version_file:
                version = version_file.read()
        except:
            version = "0.1.0 unknown"
    window = Tk()
    window.wm_title("RAPID")

    settings = load_settings()

    # settings_dialog = Toplevel(window)

    # id_var = StringVar(window)
    # id_var.set(str(settings[0]))

    # frame = Frame(settings_dialog)
    # frame.pack(expand=1, fill=X)

    # id_frame = Frame(frame)
    # id_frame.pack(expand=1, fill=X)

    # id_label = Label(id_frame, text="ID")
    # id_label.pack(side=LEFT)

    # id_box = Entry(id_frame, textvariable=id_var)
    # id_box.pack(side=LEFT, expand=1, fill=BOTH)

    # api_frame = Frame(frame)
    # api_frame.pack(expand=1, fill=BOTH)

    # api_label = Label(api_frame, text="API key")
    # api_label.pack(side=LEFT)

    # api_key_var = StringVar(window)
    # api_key_var.set(str(settings[1]))
    # api_key_box = Entry(api_frame, textvariable=api_key_var)
    # api_key_box.pack(side=LEFT, expand=1, fill=BOTH)

    # k_frame = Frame(frame)
    # k_frame.pack(expand=1, fill=BOTH)

    # k_label = Label(k_frame, text="k factor")
    # k_label.pack(side=LEFT)

    # k_var = StringVar(window)
    # k_var.set(str(settings[3]))
    # k_var_box = Entry(k_frame, textvariable=k_var)
    # k_var_box.pack(side=LEFT, expand=1, fill=BOTH)

    # reboot_frame = Frame(frame)
    # reboot_frame.pack(expand=1, fill=BOTH)
    # reboot_label = Label(reboot_frame, text="Reboot after how many failed contact attempts (0 for never)")
    # reboot_label.pack(side=LEFT)

    # reboot_var = StringVar(window)
    # reboot_var.set(str(settings[4]))
    # reboot_var_box = Entry(reboot_frame, textvariable=reboot_var)
    # reboot_var_box.pack(side=LEFT, expand=1, fill=BOTH)

    # save_settings_button = Button(frame, text="Save settings", command=on_save_click)
    # save_settings_button.pack()
    ports = get_ports()
    choice_var = StringVar(window)
    try:
        choice_var.set(ports[-1])
    except IndexError as e:
        debug_log("Ports not located", str(e))
        ports = [""]
    choice_args = (window, choice_var) + tuple(ports)
    choice = apply(OptionMenu, choice_args)
    choice.pack()
    text = Text(window)
    text.insert(END, "RAPID version: " + version + "\n")
    text.insert(END, "Logging with ID: " + settings[0] + "\n")
    text.pack(expand=1, fill=BOTH)
    button = Button(window, text="Go", command=on_go_click)
    button.pack()
    stop_btn = Button(window, text="Stop", command=on_stop_click)
    stop_btn.config(state="disabled")
    stop_btn.pack()
    flag = threading.Event()
    transmit_flag = threading.Event()

    menu = Menu(window)
    # menu.add_command(label="File", command=None)
    window.config(menu=menu)

    filemenu = Menu(menu, tearoff=0)
    filemenu.add_command(label="Settings", command=create_settings_dialog)
    menu.add_cascade(label="File", menu=filemenu)

    print load_settings()

    if settings[5] == "":
        text.insert(END, "No target URL has been set. RAPID will operate in local mode.\n")

    if os.path.exists("/home/pi/dylospi/datastore.BACKUP"):
        with open("/home/pi/dylospi/datastore.BACKUP", 'rb') as data_backup:
            try:
                datastore = pickle.load(data_backup)
                text.insert(END, "Data loaded from last run: " + str(datastore) + "\n")
            except Exception as e:
                try:
                    text.insert(END, "Error: could not load datastore, perhaps it's been corrupted? Dumping. Exception: " + str(e))
                except:
                    text.insert(END, "Error: could not load datastore, perhaps it's been corrupted? Dumping. Additionally an exception occurred trying to write the exception string to the text widget. \n")
        os.remove("/home/pi/dylospi/datastore.BACKUP")

    if "--start" in sys.argv[1:]:
        on_go_click()
        dlog("on_go_click fired")
    dlog("Ready to go...")
    window.mainloop()
