import serial
import datetime
import os
import sys
import serial.tools.list_ports as lp
from dylos2pm25 import *
import time
import requests
import json

##def debug_log(test):
##    print test

def get_dylos_version(connection):
    connection.write('Y\r')
    version = connection.readlines()
    return version[0].strip()


def get_header(connection):
    version = get_dylos_version(connection)
    now = str(datetime.date.today().month) + '/' + \
        str(datetime.date.today().day) + '/' + \
        str(datetime.date.today().year) + ' ' + \
        str(datetime.datetime.now().hour) + ':' + \
        str(datetime.datetime.now().minute)
    HEADER = """-------------------------\r
Python Dylos v0.1\r
Unit: %s\r
Date/Time: %s\r
-------------------------\r
Particles per cubic foot\r
-------------------------\r
Date/Time, Small, Large\n\r
""" % (version, now)
    return HEADER


def print_readings(connection):
    while 1:
        result = connection.read(8)
        if result:
            print result


def print_all_data(connection):
    connection.write('D\r')
    result = connection.readlines()
    return result


def read_all_lines_one_at_a_time(connection):
    connection.write('D\r')
    while True:
        result = connection.readline()
        print result


def download_results(filename='python_dylos_log.txt', port="COM3"):
    try:
        ser = serial.Serial(port, timeout=5)
        debug_log("serial connection: " + str(ser))
    except Exception as e:
        debug_log("Exception: " + str(e))
        print "Not on port %s, exiting" % port
        return ""
    try:
        debug_log("header started")
        HEADER = get_header(ser)
        debug_log("Header: " + str(HEADER))
    except Exception as e:
        debug_log("Exception: " + str(e))
        return ""
    try:
        debug_log("Closing serial connection")
        ser.close()
        debug_log("Closed")
    except Exception as e:
        debug_log("Exception: " + str(e))
        return ""
    try:
        debug_log("Reopening serial port")
        ser = serial.Serial(port, timeout=5)
        debug_log("Port reopened")
    except Exception as e:
        debug_log("Exception: " + str(e))
        return ""
    try:
        debug_log("Downloading results")
        raw_results = print_all_data(ser) #This is raw data which lacks trailing '00' for some reason...
        results = []
        for i in raw_results:
            j = i.split(",")
            j[1] = str(int(j[1])*100)
            j[2] = str(int(j[2])*100)
            i = j[0] + "," + j[1] + "," + j[2] + "\r\n"
            results.append(i)
        debug_log("Results processed: " + str(len(results)))
        ser.close()
    except Exception as e:
        debug_log("Exception: " + str(e))
        return ""
    try:
        filen = open(filename, 'wb')
        filen.write(HEADER)
        for i in results:
            # Stops later processing freaking out (it splits by space
            # as does Dylos Logger)
            i = i.replace(',', ', ')
            filen.write(i)
    except Exception as e:
        debug_log("Exception: " + str(e))
        return ""
    return os.path.join(os.path.dirname(sys.argv[0]), filename)


def get_ports():
    """
    Returns everything with a serial connection on the system, along with
    anything emulating one.
    FIXME - this should poll each device to see if it's a Dylos and return
    only those that are.
    """
    # ports = lp.comports()
    returnable_ports = [x[0] for x in lp.comports()]
    # New Raspbian lists an AMA serial port which is no good. This helps.
    # Reversing the list makes it slightly more likely that the first item
    # will be a useful port
    returnable_ports.reverse()
    #if sys.platform.startswith("darwin"):
    return returnable_ports

def send_log_content(log_filename, dylos_id="Unknown Dylos"):
    with open(log_filename, 'rb') as log_file:
        log_text = log_file.read()
        return requests.post(
            "https://api.mailgun.net/v3/sandboxd873e86e9cae4e04a46ccd9be90d4ad1.mailgun.org/messages",
            auth=("api", "key-4d8bcdb9a9f5c5b0106bb94cb555be93"),
            data={"from": "Dylos <postmaster@sandboxd873e86e9cae4e04a46ccd9be90d4ad1.mailgun.org>",
                  "to": "Ruaraidh Dobson <ruaraidh.dobson@abdn.ac.uk>",
                  "subject": "Dylos log - " + dylos_id + " at " + datetime.datetime.now().strftime("%d/%m/%y %H:%M"),
                  "text": log_text},
            files={"message": log_file})

def sit_and_wait(port, dylos_id="Unknown Dylos"):
    ser = serial.Serial(port)
    while True:
        raw_data = ser.readline()
        data = [int(x.strip())*100 for x in raw_data.split(",")]
##        print data
##        print round(calculate_PM25_equiv(data[0] - data[1]),2)
        data_dict = {"small particles": data[0],
                     "large particles": data[1],
                     "Dylos ID": dylos_id,
                     "datetime": datetime.datetime.now().strftime("%d/%m/%y %H:%M"),
                     "PM2.5 equivalent": str(round(calculate_PM25_equiv(data[0] - data[1]),2)),
                     "large particle percentage": 100*data[1]/data[0]}
##        print data_dict
        print json.dumps(data_dict)
        try:
            print requests.post("http://ruaraidhdobson.pythonanywhere.com", json=data_dict)
        except Exception as e:
            print e
        

if __name__ == "__main__":
    ports = get_ports()
    port = ports[-1] #Avoids us getting the default terminal port
    print port
    dylos_id = "257"
    ser = serial.Serial(port, timeout=5)
    print get_dylos_version(ser)
##    read_all_lines_one_at_a_time(ser)
    sit_and_wait(port, dylos_id)
##    while True:
##        filename = "dylos_log_" + datetime.datetime.now().strftime("%d%m%y_%H%M") + ".txt"
##        results = download_results(filename, port)
##        print "Downloaded log to " + filename
##        # send_log_content(filename, dylos_id)
##        time.sleep(60*60)
