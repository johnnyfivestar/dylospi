# Changelog

## [0.9.6] - [2018-02-23]
### Added
- New settings dialogue
- User-configurable k-factor for PM2.5 conversion

### Changed
- Improved reliability including a range of bugfixes
- run.sh script will now wait 60 seconds for network connection to come up

## [0.9.2] - [2017-08-02]
### Added
- Added data transmitter thread. Data is added to a queue before being sent, and only removed from the queue after a 200 response.
- Added local_data.csv file for local logging. Not yet configurable location.
- Data is now backed up before RPi reboot and reloaded when the software comes back up

### Changed
- Formatting of settings: everything's now labeled
- Number of connection errors until a reboot is now configurable. Set to 0 to ignore (allows device-only logging)
- Settings are now in their own separate window (access through file menu)
- New Target URL setting means you can point to other servers, not just the pythonanywhere one
- Local data location setting means you can change where local data is saved

## [0.9.1] - 2017-07-23
### Added
- Raspberry Pi will reboot following five connection errors, so a connection problem (such as a disconnected 3G dongle) won't be the end of the world.
- Added changelog, readme
