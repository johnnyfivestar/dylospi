# RAPID

Use a Raspberry Pi with a Dylos monitor to transmit air quality data to a remote server.

## Getting Started

Clone this repository into your home directory and change the target URL in the source to your preferred server. To run the application, run radio_dylos.py. To have it run on boot, make run.sh user-executable and then add the following to /home/<user>/.config/lxsession/LXDE-pi/autostart: "@lxterminal -e sh /home/<user>/dylospi/run.sh"

## Prerequisites

```
Python 2.7 (not yet compatible with Python 3)
python-dev
eventlet
requests
appdirs
Raspberry Pi 3 with Raspbian Jessie or later
```
