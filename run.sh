#!/bin/bash

set -e
# python ~/dylospi/dylos_serial_pi.py
echo "Sleeping for 60 seconds to allow network interfaces to come up..."
sleep 60
python ~/dylospi/radio_dylos.py --start
# sleep 120
